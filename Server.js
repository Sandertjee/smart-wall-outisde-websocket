const WebSocket = require('ws');
var url = require('url');

const wss = new WebSocket.Server({ port: 3030 });


let infoScreenOptions = {
    "options": {
        "massMultiplier": 1,
        "availableCarts": 30
    },
    "stats": {
        "registers": 3,
        "routes": true
    }
}

// Check if connection is opened and log all messages
wss.on('connection', (ws, req) => {
    // Save ID, camera and treshold to client on connection
    SaveParamsToClient(ws, req.url);

    // whenever a screen connects, send global options to it
    wss.clients.forEach(function each(client){
        if(client.type === 'infoOutput' && ws.ID === client.ID){
            client.send(JSON.stringify(infoScreenOptions))
        }
    })

    ws.on('message', data => {
        wss.clients.forEach(function each(client) {
            // If the message is sent from a camera and if it's own source,
            if(client.type === 'input' && ws.ID === client.ID){

                var cameraID = client.ID

                // create message to send to screens
                var message = ConvertToJSON({"camera": client.ID, "customerCount": ConvertToJSON(data).customerCount})

                // send message to screens
                wss.clients.forEach(function each(client){
                    // if the type of connection is an info screen output, send the message to it
                    if(client.type === 'infoOutput'){
                        client.send(JSON.stringify(message))
                    }

                    // if the type of connection is an framing screen output and the camera ID matches, send 1 camera to the specified screen
                    if(client.type === 'framingOutput' && client.cameraID === cameraID){
                        client.send(JSON.stringify(message))
                    }
                })
            }
        });
    })
})


// Save ID, Treshold and cameraID in client instance based on URL parameters
function SaveParamsToClient(ws, URL) {
    if(ws === null) {
        return false;
    }

    if(URL === "" || URL === "/") {
        return false;
    }

    // Get individual parameters from URL
    const { query: {ID, type, treshold, cameraID } } = url.parse(URL, true);
    // Set the values for the client
    if(ID !== undefined) {
        ws.ID = ID;
    }

    if(type !== undefined) {
        ws.type = type;
    }

    if(treshold !== undefined) {
        ws.treshold = treshold;
    }

    if(cameraID !== undefined) {
        ws.cameraID = cameraID;
    }

    return true;
}

// Convert the messages from the clients to JSON
function ConvertToJSON(dataInput) {
    // Check if dataInput already is a JSON string, if not convert it
    dataInput = typeof dataInput !== "string" ? JSON.stringify(dataInput) : dataInput;
    
    try {
        dataInput = JSON.parse(dataInput);
    } catch (e) {
        console.error(e);
        return false;
    }

    if(typeof dataInput === "object" && dataInput !== null) {
        return dataInput;
    }

    return false;
}

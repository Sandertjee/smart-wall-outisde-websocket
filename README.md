# README #

This README documents which steps to take in order to run the WebSocket server between the camera-tracking and React front-end.

### BASIC INFORMATION ###

* Developers: Bob Varwyk, Sander van der Burgt
* Version 1.1
* Language : node.js (javascript)

### How do I get set up? ###

* Clone repository to local machine
* Navigate to the folder in your command line
* Run the NPM install command
* Run the Node Server command
* Test the WebSocket Server

### Packages ###

* WS (NPM install ws)